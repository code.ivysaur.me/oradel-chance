# oradel-chance

![](https://img.shields.io/badge/written%20in-Javascript-blue)

Calculate attack probabilities in the board game "Oracle of Delphi".

## Changelog

2018-03-24
- Fix off-by-one in monster HP
- Remove jQuery dependency

2017-04-08
- Initial public release


## Download

- [⬇️ oradel-chance.html](dist-archive/oradel-chance.html) *(4.18 KiB)*
